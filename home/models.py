from django.db import models

class Ciudad(models.Model):
    numero_ciudad = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=80)

    def __str__(self):
        return self.name

#MODEL STADIUM
class Estadio(models.Model):
    numero_estadio = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=80)

    def __str__(self):
        return self.name

#MODEL TEAM
class Equipo(models.Model):
    numero_equipo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=80)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE, null=True)
    estadio = models.ForeignKey(Estadio, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name