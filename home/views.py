from django.shortcuts import render
from .models import Ciudad
from .models import Estadio 
from .models import Equipo

def ciudad_list(request):
    ciudades = Ciudad.objects.all()
    return render(request, 'ciudad_list.html', {'ciudades': ciudades})

def ciudad_detail(request, numero_ciudad):
    ciudad = Ciudad.objects.get(pk=numero_ciudad)
    return render(request, 'ciudad_detail.html', {'ciudad': ciudad})

def estadio_list(request):
    estadios = Estadio.objects.all()
    return render(request, 'estadio_list.html', {'estadios': estadios})

def estadio_detail(request, numero_estadio):
    estadio = Estadio.objects.get(pk=numero_estadio)
    return render(request, 'estadio_detail.html', {'estadio': estadio})

def equipo_list(request):
    equipos = Equipo.objects.all()
    return render(request, 'equipo_list.html', {'equipos': equipos})

def equipo_detail(request, numero_equipo):
    equipo = Equipo.objects.get(pk=numero_equipo)
    return render(request, 'equipo_detail.html', {'equipo': equipo})
