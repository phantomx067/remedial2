
from django.contrib import admin
from django.urls import path
from home import views

urlpatterns = [
    path('ciudades/', views.ciudad_list, name='ciudad_list'),
    path('ciudades/<int:numero_ciudad>/', views.ciudad_detail, name='ciudad_detail'),
    path('estadios/', views.estadio_list, name='estadio_list'),
    path('estadios/<int:numero_estadio>/', views.estadio_detail, name='estadio_detail'),
    path('equipos/', views.equipo_list, name='equipo_list'),
    path('equipos/<int:numero_equipo>/', views.equipo_detail, name='equipo_detail'),
    # Add other URLs as needed
]